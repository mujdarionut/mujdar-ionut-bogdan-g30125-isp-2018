package g30125.mujdar.ionut.bogdan.l6.e1;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    Shape[] shapes = new Shape[10];
    

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    public void deleteById(String id)
    {
        for(int i=0;i<shapes.length;i++) {
            if (shapes[i] != null)
            {
                if(id.equals(shapes[i].getId()))
                {
                    System.out.println("ID Gasit");
                    shapes[i]=null;
                    break;
                }
            }
        }
        this.repaint();

    }
    public void addShape(Shape s1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = s1;
                break;
            }
        }
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }

    }
}