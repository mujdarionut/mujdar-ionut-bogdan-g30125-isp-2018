package g30125.mujdar.ionut.bogdan.l4.e6;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import g30125.mujdar.ionut.bogdan.l4.e4.*;


public class TestBook {
	
	@Test
	public void testToString() {
		Author[] a = new Author[2];
		a[0]=new Author("autor1","autor1@yahoo.com",'m');
				
		a[1]=new Author("autor2","autor2@yahoo.com",'f');
		Book b = new Book("b1",a,10,3);
		
		assertEquals("book name b1 by 2",b.toString());
		
	}
	
	@Test
	public void testPrintAuthors() {
		Author[] a = new Author[2];
		a[0]=new Author("autor1","autor1@yahoo.com",'m');
				
		a[1]=new Author("autor2","autor2@yahoo.com",'f');
		Book b1 = new Book("b1",a,10,3);
		b1.printAuthors();
		assertEquals("autor1",a[0].getName());
		assertEquals("autor2",a[1].getName());
		
		
	}

}