package g30125.mujdar.ionut.bogdan.l4.e4;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAuthor {
	@Test
	public void shouldPrintString() {
		Author autor1 = new Author("autor1","autor1@email.com",'m');
		assertEquals("Author autor1 ( m ) at email autor1@email.com",autor1.toString());
	}
}
