package g30125.mujdar.ionut.l3.e6;

public class Mypoint {
	private int x;
	private int y;
	
	public Mypoint()
	{
		x=0;
		y=0;	
	}
	
	
	public Mypoint(int cx, int cy)
	{
		x=cx;
		y=cy;
	}
	
	
	public int getX()
    {
        return x;
    }
    public int getY()
    {
        return y;
    }

    public int setX(int x)
    {
      return  this.x = x;
    }
    public int setY(int y)
    {
      return  this.y = y;
    }
    
    public void setXY(int cx, int cy)
    {
    	this.x=cx;
    	this.y=cy;
    }
    
  
    void ShowXY() {
        System.out.println("("+x+","+y+")");
    }
    

    public double distance(int cx, int cy)
    {
    	return Math.sqrt((Math.pow(cx-x, 2)+Math.pow(cy-y, 2)));
    }
    
    
    public double distance(Mypoint c)
    {
    	return Math.sqrt((Math.pow(c.x-x, 2)+Math.pow(c.y-y, 2)));
    }


}
