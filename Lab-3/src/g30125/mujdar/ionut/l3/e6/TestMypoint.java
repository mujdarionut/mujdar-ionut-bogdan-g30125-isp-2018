package g30125.mujdar.ionut.l3.e6;
public class TestMypoint {
	 public static void main(String[] args) {
		
		 Mypoint p1 = new Mypoint();
		 Mypoint p2 = new Mypoint();
		 
		
		 p1 = new Mypoint(2,3);
		 System.out.println("Arg.Constructor: "+p1.getX()+","+p1.getY());
		 
		 
		 p1.ShowXY();
		 
		 
		 p1.setXY(4, 2);
		 p1.ShowXY();
		 
		 
		 p2.setX(5);
		 p2.ShowXY();
		 p2.setY(9);
		 p2.ShowXY();
		 
		
		 System.out.println("Distanta dintre puncte: "+p1.distance(2, 6));
		 
		 
		 System.out.println("Distanta dintre puncte: "+p1.distance(p2));
	 }

}
