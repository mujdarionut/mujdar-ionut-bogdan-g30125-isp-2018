package g30125.mujdar.ionut.l3.e3;

import becker.robots.*;

public class ex3
{
   public static void main(String[] args)
   {  
   	
   	City prague = new City();
      Thing parcel = new Thing(prague, 1, 2);
      Robot karel = new Robot(prague, 1, 1, Direction.NORTH);
 
		
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
    
      karel.turnLeft();
      karel.turnLeft();	
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      
   }
} 