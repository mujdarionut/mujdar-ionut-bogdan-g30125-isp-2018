package g30125.mujdar.ionut.bogdan.l7.e2;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		ArrayList<BankAccount> l = new ArrayList<>();
		BankAccount bank1 = new BankAccount("Da1",150);
		BankAccount bank2 = new BankAccount("Da2",150);
		
		l.add(bank1);
		l.add(bank2);
		for(BankAccount a:l)
			System.out.println(a.toString());
	}
	
}
